# mgi_teste_tecnico

## Descrição do Projeto
Este projeto é parte de uma aplicação web que permite o cadastro de usuários. 
O backend foi desenvolvido utilizando Java com o framework Spring. 
Ele fornece uma API RESTful para lidar com as solicitações de registro de usuários, 
validando os dados recebidos, armazenando-os em um banco de dados PostgreSQL e retornando 
respostas apropriadas para o frontend.

# Tecnologias Utilizadas
- Java: Linguagem de programação principal.
- Spring Boot: Framework utilizado para desenvolvimento da aplicação.
- Spring Data JPA: Facilita o acesso e manipulação de dados no banco de dados.
- Spring MVC: Para criar a API RESTful.
- PostgreSQL: Banco de dados utilizado para armazenar os dados dos usuários.


## Pré Requisitos

- Java 17
- Banco de Dados PostgreSQL
- Maven 4.0.0
- Docker

## Montagem de Ambiente

`````
docker-compose up -d
`````
Isso irá subir os serviços de Banco de Dados, Kafka, zookeeper e backend.<br>

# [Swagger](http://localhost:8080/swagger-ui/index.html)
`````
http://localhost:8080/swagger-ui/index.html
`````