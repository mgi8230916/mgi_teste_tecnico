package br.com.braz.mgi.doman.dto;

import jakarta.validation.ConstraintViolation;
import jakarta.validation.Validation;
import jakarta.validation.Validator;
import jakarta.validation.ValidatorFactory;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

public class UsuarioRequestDTOTest {

    private Validator validator;

    @BeforeEach
    void setUp() {
        ValidatorFactory factory = Validation.buildDefaultValidatorFactory();
        validator = factory.getValidator();
    }

    @Test
    void testValidUsuarioRequestDTO() {

        UsuarioRequestDTO usuarioDTO = new UsuarioRequestDTO("Nome12", "email@example.com", "senha123", "senha123");


        Set<ConstraintViolation<UsuarioRequestDTO>> violations = validator.validate(usuarioDTO);

        // Then
        assertTrue(violations.isEmpty());
    }

    @Test
    void testInvalidUsuarioRequestDTO_NOME_MINIMO() {

        UsuarioRequestDTO usuarioDTO = new UsuarioRequestDTO("N", "email@tes.com", "senha123", "senha123");

        Set<ConstraintViolation<UsuarioRequestDTO>> violations = validator.validate(usuarioDTO);

        List<String> violationMessages = violations.stream()
                .map(ConstraintViolation::getMessage)
                .collect(Collectors.toList());

        assertEquals("Obrigatório, mínimo de 3 caracteres, máximo de 50 caracteres", violationMessages.get(0));
    }

    @Test
    void testInvalidUsuarioRequestDTO_NOME_MAXIMO() {

        UsuarioRequestDTO usuarioDTO = new UsuarioRequestDTO("000000000000000000600000000000000000000000000000051", "email@tes.com", "senha123", "senha123");

        Set<ConstraintViolation<UsuarioRequestDTO>> violations = validator.validate(usuarioDTO);

        List<String> violationMessages = violations.stream()
                .map(ConstraintViolation::getMessage)
                        .toList();

        assertEquals("Obrigatório, mínimo de 3 caracteres, máximo de 50 caracteres", violationMessages.get(0));
    }

    @Test
    void testInvalidUsuarioRequestDTO_SENHA_MINIMA() {
        UsuarioRequestDTO usuarioDTO = new UsuarioRequestDTO("NOME_NORMAL", "email@tes.com", "1234", "12345");

        Set<ConstraintViolation<UsuarioRequestDTO>> violations = validator.validate(usuarioDTO);

        List<String> violationMessages = violations.stream()
                .map(ConstraintViolation::getMessage)
                .collect(Collectors.toList());

        assertEquals("Obrigatória, mínimo de 6 caracteres, máximo de 20 caracteres", violationMessages.get(0));
    }

    @Test
    void testInvalidUsuarioRequestDTO_SENHA_MAXIMA() {

        UsuarioRequestDTO usuarioDTO = new UsuarioRequestDTO("NOME_NORMAL", "email@tes.com", "0000000000000000000021", "0000000000000000000021");

        Set<ConstraintViolation<UsuarioRequestDTO>> violations = validator.validate(usuarioDTO);

        List<String> violationMessages = violations.stream()
                .map(ConstraintViolation::getMessage)
                .collect(Collectors.toList());

        assertEquals("Obrigatória, mínimo de 6 caracteres, máximo de 20 caracteres", violationMessages.get(0));
    }

    @Test
    void testInvalidUsuarioRequestDTO_EMAIL_INVALIDO() {

        UsuarioRequestDTO usuarioDTO = new UsuarioRequestDTO("NOME_NORMAL", "emailXXXXXXXXX", "SENHA_NORMAL", "SENHA_NORMAL");

        Set<ConstraintViolation<UsuarioRequestDTO>> violations = validator.validate(usuarioDTO);

        List<String> violationMessages = violations.stream()
                .limit(3)
                .map(ConstraintViolation::getMessage)
                .collect(Collectors.toList());

        assertEquals("Fomato de email inválido", violationMessages.get(0));
    }
}
