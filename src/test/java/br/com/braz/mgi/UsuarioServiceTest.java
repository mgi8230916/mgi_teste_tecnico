package br.com.braz.mgi;

import br.com.braz.mgi.doman.dto.UsuarioRequestDTO;
import br.com.braz.mgi.doman.entity.UsuarioEntity;
import br.com.braz.mgi.repository.UsuarioRepository;
import br.com.braz.mgi.service.UsuarioService;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.*;

class UsuarioServiceTest {

    private UsuarioService usuarioService;

    @Mock
    private UsuarioRepository usuarioRepository;

    @BeforeEach
    void setUp() {
        MockitoAnnotations.openMocks(this);
        usuarioService = new UsuarioService(usuarioRepository);
    }

    @Test
    void testCreateUsuario_Success() {
        
        UsuarioRequestDTO usuarioDTO = new UsuarioRequestDTO("nome", "email@email.com", "senha123", "senha123");
        UsuarioEntity usuarioEntity = UsuarioEntity.builder()
                .nome(usuarioDTO.nome())
                .email(usuarioDTO.email())
                .senha(usuarioDTO.senha())
                .build();
        when(usuarioRepository.save(any(UsuarioEntity.class))).thenReturn(usuarioEntity);

        
        UsuarioEntity createdUsuario = usuarioService.createUsuario(usuarioDTO);

        // Then
        assertNotNull(createdUsuario);
        assertEquals(usuarioEntity.getNome(), createdUsuario.getNome());
        assertEquals(usuarioEntity.getEmail(), createdUsuario.getEmail());
        assertEquals(usuarioEntity.getSenha(), createdUsuario.getSenha());
        verify(usuarioRepository, times(1)).save(any(UsuarioEntity.class));
    }
}
