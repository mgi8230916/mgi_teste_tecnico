package br.com.braz.mgi.exceptions.global;

import br.com.braz.mgi.exceptions.ErrorMensagens;
import br.com.braz.mgi.exceptions.ExecessaoLonginOff;
import org.springdoc.api.ErrorMessage;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestControllerAdvice;

import java.util.List;

@RestControllerAdvice
public class ErrorHandler {
    @ExceptionHandler(MethodArgumentNotValidException.class)
    public ResponseEntity<List<ErrorMensagens>> tratarErro400(MethodArgumentNotValidException ex) {
        return ResponseEntity
                .badRequest()
                .body(ex.getFieldErrors()
                        .stream()
                        .map(ErrorMensagens::new)
                        .toList());
    }
    @ExceptionHandler(ExecessaoLonginOff.class)
    public ResponseEntity<ErrorMessage> tratarErroDeLogin(ExecessaoLonginOff ex) {
        return ResponseEntity
                .badRequest()
                .body(new ErrorMessage(ex.getMessage()));
    }

    @ExceptionHandler(DataIntegrityViolationException.class)
    public ResponseEntity<ErrorMessage> excessaoSql(DataIntegrityViolationException ex) {
        return ResponseEntity
                .badRequest()
                .body(new ErrorMessage("Email já Cadastrado"));
    }

}
