package br.com.braz.mgi.exceptions;
import java.util.ArrayList;
import java.util.List;

public class ErrorMessage {
    private String message;
    private String localizedItem;

    public ErrorMessage(String message, String localizedItem) {
        this.message = message;
        this.localizedItem = localizedItem;
    }

}
