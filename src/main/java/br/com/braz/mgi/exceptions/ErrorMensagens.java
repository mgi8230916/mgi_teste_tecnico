package br.com.braz.mgi.exceptions;

import com.fasterxml.jackson.annotation.JsonInclude;
import org.springframework.validation.FieldError;

@JsonInclude(JsonInclude.Include.NON_NULL)
public record ErrorMensagens(String message, String localizedItem, Throwable cause, StackTraceElement[] stackTrace) {
    public ErrorMensagens(FieldError fieldError) {
        this(fieldError.getDefaultMessage(), fieldError.getField(),null,null);
    }
    public ErrorMensagens(String message, StackTraceElement[] stackTrace, Throwable cause) {
        this(message,null,cause,stackTrace);
    }
}
