package br.com.braz.mgi.exceptions;

import lombok.NoArgsConstructor;

@NoArgsConstructor
public class CriarUsuarioExcessao extends IllegalArgumentException {

    public CriarUsuarioExcessao(String message) {
        super(message);
    }
}
