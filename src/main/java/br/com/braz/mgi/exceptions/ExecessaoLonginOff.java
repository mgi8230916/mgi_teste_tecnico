package br.com.braz.mgi.exceptions;

import lombok.NoArgsConstructor;
import org.springframework.web.bind.annotation.RestControllerAdvice;

@NoArgsConstructor
@RestControllerAdvice
public class ExecessaoLonginOff extends RuntimeException {
    public ExecessaoLonginOff(String message) {
        super(message);
    }
}
