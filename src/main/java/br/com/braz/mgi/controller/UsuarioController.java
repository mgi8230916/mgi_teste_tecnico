package br.com.braz.mgi.controller;

import br.com.braz.mgi.doman.dto.UsuarioRequestDTO;
import br.com.braz.mgi.doman.dto.UsuarioResponseDTO;
import br.com.braz.mgi.doman.entity.UsuarioEntity;
import br.com.braz.mgi.service.UsuarioService;
import io.swagger.v3.oas.annotations.Operation;

import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.responses.ApiResponses;
import jakarta.validation.Valid;
import lombok.RequiredArgsConstructor;

import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.util.UriComponentsBuilder;

@RequiredArgsConstructor
@RestController
@RequestMapping(value = "/usuarios", produces = MediaType.APPLICATION_JSON_VALUE)
public class UsuarioController {

    private final UsuarioService usuarioService;

    /**
     * @param usuario
     * @param uriBuilder
     * @return
     */
    @Operation(
            summary = "criação de Usúario",
            description = "cria um usúario com com atraves do dto contendo nome email e senha com requisitos minimo para o banco"
    )
    @ApiResponses({
            @ApiResponse(responseCode = "201", content = {@Content(schema = @Schema(implementation = UsuarioResponseDTO.class), mediaType = "application/json")}),
            @ApiResponse(responseCode = "400", content = {@Content(schema = @Schema())})
    })
    @PostMapping
    public ResponseEntity<UsuarioResponseDTO> createUsuario(@RequestBody @Valid UsuarioRequestDTO usuario,
                                                            UriComponentsBuilder uriBuilder) {
        UsuarioEntity createdUsuario = usuarioService.createUsuario(usuario);
        return ResponseEntity.created(uriBuilder.path("/usuarios/{id}").
                buildAndExpand(createdUsuario.getId()).toUri()).body(new UsuarioResponseDTO(createdUsuario));
    }

}