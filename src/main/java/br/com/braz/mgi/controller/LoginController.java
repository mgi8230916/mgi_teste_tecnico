package br.com.braz.mgi.controller;


import br.com.braz.mgi.doman.dto.LoginRequestDTO;
import br.com.braz.mgi.service.LoginService;
import jakarta.validation.Valid;
import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RequiredArgsConstructor
@RestController
@RequestMapping("/login")
public class LoginController {

    private final LoginService loginService;
    @PostMapping
    public ResponseEntity<Boolean> login(@RequestBody @Valid LoginRequestDTO loginRequest) {
        return ResponseEntity.ok(loginService.validaUsuario(loginRequest));
    }

}
