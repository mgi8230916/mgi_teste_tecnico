package br.com.braz.mgi;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class MgiApplication {

	public static void main(String[] args) {
		SpringApplication.run(MgiApplication.class, args);
	}

}
