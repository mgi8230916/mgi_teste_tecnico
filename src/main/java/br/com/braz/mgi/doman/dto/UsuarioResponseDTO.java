package br.com.braz.mgi.doman.dto;

import br.com.braz.mgi.doman.entity.UsuarioEntity;

public record UsuarioResponseDTO(String nome,
                                 String email) {
    public UsuarioResponseDTO(UsuarioEntity usuarioEntity){
        this(usuarioEntity.getNome(),usuarioEntity.getEmail());
    }
}
