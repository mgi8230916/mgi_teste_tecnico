package br.com.braz.mgi.doman.entity;

import jakarta.persistence.*;
import jakarta.validation.constraints.Size;
import lombok.*;

@NoArgsConstructor
@AllArgsConstructor
@Builder
@Getter
@Setter
@Entity
@Table(name = "USUARIO")
public class UsuarioEntity {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    @Column(name = "nome", nullable = false)
    @Size(min = 3, max = 50)
    private String nome;
    @Column(name = "senha", nullable = false)
    @Size(min = 6, max = 20)
    private String senha;
    @Column(name = "email", nullable = false, unique = true)
    private String email;


}
