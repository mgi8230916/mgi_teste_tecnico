package br.com.braz.mgi.doman.dto;

import br.com.braz.mgi.anotacoes.ValidaEmail;
import jakarta.validation.constraints.Size;

/**
 * Request é o nome dado para a entrada da Raquisição usadado para Imput
 *
 * @param email
 * @param senha
 */
public record LoginRequestDTO(

        @ValidaEmail
        String email,
        @Size(min = 6, max = 20, message = "Obrigatória, mínimo de 6 caracteres, máximo de 20 caracteres")
        String senha) {
}
