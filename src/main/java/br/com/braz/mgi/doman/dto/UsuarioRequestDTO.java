package br.com.braz.mgi.doman.dto;

import br.com.braz.mgi.anotacoes.ValidaEmail;
import jakarta.validation.constraints.Size;

/**
 * Request é o nome dado para a entrada da Raquisição usadado para Imput
 *
 * @param nome
 * @param email
 * @param senha
 */
public record UsuarioRequestDTO(
        @Size(min = 2, max = 50, message = "Obrigatório, mínimo de 3 caracteres, máximo de 50 caracteres")
        String nome,
        @ValidaEmail(message = "Fomato de email inválido")
        String email,
        @Size(min = 5, max = 20, message = "Obrigatória, mínimo de 6 caracteres, máximo de 20 caracteres")
        String confirmaSenha,
        @Size(min = 5, max = 20, message = "Obrigatória, mínimo de 6 caracteres, máximo de 20 caracteres")
        String senha) {
}
