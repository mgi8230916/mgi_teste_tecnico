package br.com.braz.mgi.service;

import br.com.braz.mgi.doman.dto.UsuarioRequestDTO;
import br.com.braz.mgi.doman.entity.UsuarioEntity;
import br.com.braz.mgi.exceptions.ExecessaoLonginOff;
import br.com.braz.mgi.repository.UsuarioRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

@RequiredArgsConstructor
@Service
public class UsuarioService {

    private final UsuarioRepository usuarioRepository;

    public UsuarioEntity createUsuario(UsuarioRequestDTO usuario) {
        try {
            return usuarioRepository.save(UsuarioEntity.builder()
                    .email(usuario.email())
                    .senha(usuario.senha())
                    .nome(usuario.nome())
                    .build());
        } catch (IllegalArgumentException ex) {
            throw new IllegalArgumentException("Erro ao criar usuário");
        }
    }

}
