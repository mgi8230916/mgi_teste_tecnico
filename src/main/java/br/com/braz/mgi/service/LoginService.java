package br.com.braz.mgi.service;


import br.com.braz.mgi.doman.dto.LoginRequestDTO;
import br.com.braz.mgi.doman.entity.UsuarioEntity;
import br.com.braz.mgi.exceptions.ExecessaoLonginOff;
import br.com.braz.mgi.repository.UsuarioRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.Objects;
import java.util.Optional;

@RequiredArgsConstructor
@Service
public class LoginService {

    private final UsuarioRepository usuarioRepository;
    public boolean validaUsuario(LoginRequestDTO loginRequest) {
        UsuarioEntity entity = usuarioRepository.findByEmailAndSenha(loginRequest.email(), loginRequest.senha())
                .orElseThrow(() -> new ExecessaoLonginOff("Usúario ou senha inválida"));

         return !Objects.isNull(entity);
    }
}
